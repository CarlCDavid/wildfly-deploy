Wildfly Hot Deployment
================

[![Join the chat at https://gitter.im/vasanthkg/wildfly-deploy](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/vasanthkg/wildfly-deploy?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![Build Status](https://travis-ci.org/vasanthkg/wildfly-deploy.svg?branch=master)](https://travis-ci.org/vasanthkg/wildfly-deploy)

This perl scripts used to deploy the Jenkins Artifacts to wildfly.
